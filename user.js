
/**
 * Created by userluser on 13.04.17.
 */

var accel = 0.08;
var curMouseX = 0;
var curMouseY = 0;
var targetMouseX = 0;
var targetMouseY = 0;
var wX = $(window).width();
var wY = $(window).height();


$('body').bind('mousemove', function(e){
    targetMouseX = e.pageX - wX/1.5;
    targetMouseY = e.pageY - wY/1.5;
})

setInterval(function(){
    curMouseX += (targetMouseX - curMouseX) * accel;
    curMouseY += (targetMouseY - curMouseY) * accel;

    $('#foreground').css({
        left: -curMouseX * $('#foreground').attr('data-speed'),
        top: -curMouseY * $('#foreground').attr('data-speed')
    })

    $('#background').css({
        left: curMouseX * $('#background').attr('data-speed'),
        top: curMouseY * $('#background').attr('data-speed')
    })
},20);




